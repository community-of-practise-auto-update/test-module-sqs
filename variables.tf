variable "NAME" {
  description = "Provide name of the sqs bucket"
  type        = string
}

variable "DELAY_SECONDS" {
  description = "Provide delay second"
  type        = number
}

variable "MAX_MESSAGE_SIZE" {
  description = "Provide max message size"
  type        = number
}

variable "MESSAGE_RETENTION_SECONDS" {
  description = "Provide max message size"
  type        = number
}

variable "RECEIVE_WAIT_TIME_SECONDS" {
  description = "Provide receive wait seconds"
  type        = number
}
