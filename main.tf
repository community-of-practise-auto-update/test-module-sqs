resource "aws_sqs_queue" "terraform_queue" {
  name                      = var.NAME
  delay_seconds             = var.DELAY_SECONDS
  max_message_size          = var.MAX_MESSAGE_SIZE
  message_retention_seconds = var.MESSAGE_RETENTION_SECONDS
  receive_wait_time_seconds = var.RECEIVE_WAIT_TIME_SECONDS

  tags = {
    Environment = "dev"
  }
}

resource "aws_sqs_queue" "terraform_queue_deadletter" {
  name = "${var.NAME}-deadletter"
  redrive_allow_policy = jsonencode({
    redrivePermission = "byQueue",
    sourceQueueArns   = [aws_sqs_queue.terraform_queue.arn]
  })
}
